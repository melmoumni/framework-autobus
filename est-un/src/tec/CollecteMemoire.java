package tec;
import java.util.LinkedList;


public class CollecteMemoire implements Collecte {
    int numero_arret = 0;
    int nbEntrees = 0;
    int nbSorties = 0;
    LinkedList<Integer> entrees = new LinkedList<Integer>();
    LinkedList<Integer> sorties = new LinkedList<Integer>();

    public CollecteMemoire(){
    }

    public void uneEntree(){
        nbEntrees++;
    }

    public void uneSortie(){
        nbSorties++;
    }

    public void changerArret(){
        numero_arret++;
        entrees.add(nbEntrees);
        sorties.add(nbSorties);
        nbEntrees = 0;
        nbSorties = 0;
    }

    public String toString() {
        int i;
        String str = "";

        for (i = 0; i < numero_arret; i++) {
            str += "numero arret : " + (i+1) + ", nombre d'entrées : " + entrees.get(i) + ", nombre de sorties : " + sorties.get(i) + "\n";
        }
        return str;
    }

}
