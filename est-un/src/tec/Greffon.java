package tec;

import tec.Bus;
import tec.Transport;
import tec.Collecte;
import tec.CollecteMemoire;
import tec.Autobus;

public class Greffon extends Autobus{
    Collecte c;
    public Greffon(int assis, int debout){
	super(assis, debout);
	//c = new CollecteFichier();
    }
    public Greffon(int assis, int debout, Collecte co){
	super(assis, debout);
	c = co;
    }
    @Override
    public void demanderPlaceAssise(Passager p){
	super.demanderPlaceAssise(p);
	if(p.estAssis())
	    c.uneEntree();
    }
    @Override
    public void demanderPlaceDebout(Passager p){
	super.demanderPlaceDebout(p);
	if(p.estDebout())
	    c.uneEntree();
    }
    @Override
    public void demanderSortie(Passager p){
	super.demanderSortie(p);
	if(p.estDehors())
	    c.uneSortie();
    }
    @Override
    public void allerArretSuivant(){ 
	try{
	    super.allerArretSuivant();
	    c.changerArret();
	}
	catch(TecInvalidException e){}
    }

    public String toString() {
        return super.toString();
    }
   
}


