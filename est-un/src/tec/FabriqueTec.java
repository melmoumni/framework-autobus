package tec;

import tec.PassagerAbstrait;
import tec.CaractereArret;
import tec.PassagerStandard;
import tec.PassagerLunatique;
import tec.PassagerStresse;
import tec.Calme;
import tec.Prudent;
import tec.Nerveux;
import tec.Greffon;
import tec.Collecte;
import tec.CollecteMemoire;
import tec.CollecteFichier;

public abstract class FabriqueTec {
    private FabriqueTec() {
    }
    
    public static PassagerAbstrait fairePassagerStandard(String nom, int destination) {
	Calme c = new Calme();
        return new PassagerStandard(nom, destination, c);
    }

    public static PassagerAbstrait fairePassagerStresse(String nom, int destination) {
	Prudent c = new Prudent();
        return new PassagerStresse(nom, destination, c);
    }
    public static PassagerAbstrait fairePassagerLunatique(String nom, int destination) {
	Nerveux c = new Nerveux();
        return new PassagerLunatique(nom, destination, c);
    }
    
    public static Autobus faireAutobus(int assis, int debout){
	
	return new Autobus(assis,debout);
    } 

    public static Greffon faireGreffon(int assis, int debout, Collecte co){
	
	return new Greffon(assis, debout, co);
    }
    
    public static CollecteMemoire faireCollecteMemoire(){
	
	return new CollecteMemoire();
    }
    public static CollecteFichier faireCollecteFichier(String s){
	
	return new CollecteFichier(s);
    }
}
