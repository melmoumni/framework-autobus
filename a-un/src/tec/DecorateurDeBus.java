package tec;

import tec.Bus;

abstract class DecorateurDeBus implements Bus {
    public Bus that;

    public DecorateurDeBus() {
        that = this;
    }

    public void setThat(Bus b) {
        that = b;
    }
}
