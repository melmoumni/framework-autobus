package tec;

import java.io.Writer;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;



public class CollecteFichier implements Collecte{
    String path;
    PrintWriter p;
    FileWriter f;
    int numero_arret;
    int entrees;
    int sorties;
    public CollecteFichier(String s){
	numero_arret = 0;
	path = s;
    }
    public void uneEntree(){
	entrees ++;
    }
    public void uneSortie(){
	sorties ++;	
    }
    public void changerArret(){
	ecrire("numero arret : "+numero_arret);
	ecrire("nombre d'entrees : "+entrees);
	ecrire("nombre de sorties : "+sorties);
	numero_arret++;
	entrees = 0;
	sorties = 0;
    }

    public void open()
    {
	try{
	    f = new FileWriter(path, true);
	    p = new PrintWriter(f);
	}
	catch(IOException e){
	}
    }
    
    public void close() {
	try{
	    p.flush();
	    f.close();
	}
	catch(IOException e){
	}
    }
    public void ecrire(String s){
	open();
	p.println(s);
	System.out.println(s);
	close();
    }
}
