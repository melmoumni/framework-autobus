package tec;

import tec.Bus;
import tec.Transport;
import tec.Collecte;
import tec.CollecteMemoire;
import tec.Autobus;
import tec.DecorateurDeBus;

public class Greffon implements Bus, Transport{
    Bus b;
    Collecte c;

    public Greffon(Transport t, Collecte co){
	b = (Bus)t;

        if (b instanceof DecorateurDeBus) {
            DecorateurDeBus d = (DecorateurDeBus) b;
            Bus that = this;
            d.setThat(that);
        }

	c = co;
    }
    public boolean aPlaceAssise(){
	return b.aPlaceAssise();
    }
    public boolean aPlaceDebout(){
	return b.aPlaceDebout();
    }

    public void demanderPlaceAssise(Passager p){
	b.demanderPlaceAssise(p);
	if(p.estAssis())
	    c.uneEntree();
    }
    public void demanderPlaceDebout(Passager p){
	b.demanderPlaceDebout(p);
	if(p.estDebout())
	    c.uneEntree();
    }
    public void demanderSortie(Passager p){
	b.demanderSortie(p);
	if(p.estDehors())
	    c.uneSortie();
    }

    public void demanderChangerEnDebout(Passager p){

	b.demanderChangerEnDebout(p);
    }
    public void demanderChangerEnAssis(Passager p){

	b.demanderChangerEnAssis(p);
    }

    public int distanceA(int numeroArret){
	return b.distanceA(numeroArret);
    }
    public void allerArretSuivant(){ 
	Transport t = (Transport) b;
	try{
	    t.allerArretSuivant();
	    c.changerArret();
	}
	catch(TecInvalidException e){}
    }
    
    public String toString() {
        return b.toString();
    }
}


